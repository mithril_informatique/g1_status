class WelcomeController < ApplicationController

    def index
        @service_types = ServiceType.all.order(:position)
    end

    def about
        
    end

end