class Service < ApplicationRecord
  belongs_to :service_type
  has_many :histories, :dependent => :delete_all

  def uptime
    histories = self.histories.last(30)
    nb_ping = histories.count
    positive_ping = (histories.select{|history| history.ping == true}).count

    return nb_ping.positive? ? (positive_ping*100)/nb_ping : 0
  end

  def ping
    case service_type.name
    when 'Duniter'
      self.ping_duniter
    when 'Cesiumplus'
      self.ping_cesiumplus
    when 'WotWizard'
      self.ping_wotwizard
    else
      self.ping_other
    end
  end

  def ping_duniter
    number = nil
    duration = nil
    begin
      requete = RestClient::Request.execute(:method => :get, :url => "#{self.ssl ? 'https://' : 'http://'}#{self.url}:#{self.port}/blockchain/current", :timeout => 10, :open_timeout => 10)
      duration = (requete.duration * 1000).to_i
      number = JSON.parse(requete.body)["number"]
    rescue
      puts "Impossible de joindre l'API Duniter pour #{self.url}"
    end

    begin
      requete = RestClient::Request.execute(:method => :get, :url => "#{self.ssl ? 'https://' : 'http://'}#{self.url}:#{self.port}", :timeout => 10, :open_timeout => 10)
      self.version = JSON.parse(requete.body).first[1]["version"]
      self.save
    rescue
      puts "Impossible de récupérer la version Duniter pour #{self.url}"
    end

    History.create(date: DateTime.now, service_id: self.id, block: number, duration: duration)
  end

  def ping_cesiumplus
    duration = nil
    nb_documents = 0

    begin
      requete = RestClient::Request.execute(:method => :get, :url => "#{self.ssl ? 'https://' : 'http://'}#{self.url}:#{self.port}/node/summary", :timeout => 10, :open_timeout => 10)
      self.version = JSON.parse(requete.body).first[1]["version"]
      #self.status = true
    rescue
      #self.status = false
      puts "Impossible de récupérer la version Cesium+ pour #{self.url}"
    end

    begin
      requete = RestClient::Request.execute(:method => :get, :url => "#{self.ssl ? 'https://' : 'http://'}#{self.url}:#{self.port}/node/stats", :timeout => 10, :open_timeout => 10)
      duration = (requete.duration * 1000).to_i
      nb_documents = requete.split('docs').last.split('count')[1].split(':')[1].split(',').first.to_i
      self.status = nb_documents.positive?
    rescue
      self.status = false
      puts "Impossible de joindre l'API Cesium+ pour #{self.url}"
    end
    self.save

    History.create(date: DateTime.now, service_id: self.id, ping: self.status, nb_documents: nb_documents, duration: duration, error: self.status ? 0 : 1)
  end

  def ping_wotwizard
    begin
      client = SimpleGraphqlClient::Client.new(url: "#{self.ssl ? 'https://' : 'http://'}#{self.url}:#{self.port}")
      version = client.query(gql: %{query {version}}).version
      self.status = !version.empty?
      self.version = version
    rescue
      self.status = false
    end
    self.save

    History.create(date: DateTime.now, service_id: self.id, ping: self.status, error: self.status ? 0 : 1)
  end

  def ping_other
    ping = nil
    begin
      ping = Net::Ping::TCP.new(self.url, self.port).ping?
    rescue
      puts "Erreur de test pour #{self.url}"
      ping = false
    end
    self.status = ping
    self.save

    History.create(date: DateTime.now, service_id: self.id, ping: ping, error: self.status ? 0 : 1)
  end


  def last_block
    return nil unless self.type?('Duniter')

    self.histories.last&.block
  end

  def type?(type_name)
    self.service_type.name == type_name
  end

  def status_classe
    case self.histories.last&.error
    when 0
      return 'ok'
    when 1, 2
      return 'ko'
    else
      return 'nil'
    end
  end

  def check_service(cb=nil)
    cb = self.service_type.current_block if cb.nil?
    error = self.histories.last.define_error(cb)
    self.status = (error==0)
    self.save
  end

  def nb_documents
    self.histories.last.nb_documents
  end

end

