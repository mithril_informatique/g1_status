namespace :survey do
    desc 'Test des différents services'
    task all: :environment do
        puts "Test de tous les services"
        ServiceType.all.each do |service_type| 
            test_service_type(service_type.name)
        end
    end

    desc 'Test d\'un type de service spécifique'
    task :test, [:service_type_name] => [:environment] do |task, args|
        test_service_type(args[:service_type_name])
    end

    def test_service_type(service_type_name)
        puts "Test #{service_type_name}"
        service_type = ServiceType.where(name: service_type_name).first
        service_type.ping
        service_type.check_services_block
    end

end