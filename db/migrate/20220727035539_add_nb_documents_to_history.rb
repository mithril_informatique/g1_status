class AddNbDocumentsToHistory < ActiveRecord::Migration[7.0]
  def change
    add_column :histories, :nb_documents, :integer
  end
end
