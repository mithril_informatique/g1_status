class CreateServices < ActiveRecord::Migration[7.0]
  def change
    create_table :services do |t|
      t.string :url
      t.boolean :status
      t.integer :uptime
      t.string :ipv4
      t.string :ipv6
      t.string :admin
      t.string :server_funding
      t.references :service_type, null: false, foreign_key: true

      t.timestamps
    end
  end
end
