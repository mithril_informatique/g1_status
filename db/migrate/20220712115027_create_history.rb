class CreateHistory < ActiveRecord::Migration[7.0]
  def change
    create_table :histories do |t|
      t.datetime :date
      t.references :service, null: false, foreign_key: true
      t.string :status

      t.timestamps
    end
  end
end
