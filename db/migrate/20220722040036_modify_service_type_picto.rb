class ModifyServiceTypePicto < ActiveRecord::Migration[7.0]
  def change
    ServiceType.create name: 'WotMap', picto: 'map-fill', position: 1
    
    st = ServiceType.where(name: 'Duniter').first
    st.picto = 'link'
    st.save

    st = ServiceType.where(name: 'Cesiumplus').first
    st.picto = 'file-earmark-person-fill'
    st.save

    st = ServiceType.where(name: 'WotWizard').first
    st.picto = 'magic'
    st.save
  end
end
