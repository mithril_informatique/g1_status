class AddPortToService < ActiveRecord::Migration[7.0]
  def change
    add_column :services, :port, :integer, default: 443
  end
end
