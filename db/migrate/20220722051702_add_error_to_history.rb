class AddErrorToHistory < ActiveRecord::Migration[7.0]
  def change
    add_column :histories, :error, :integer
    remove_column :histories, :comment
  end
end
