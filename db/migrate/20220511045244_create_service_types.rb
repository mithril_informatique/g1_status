class CreateServiceTypes < ActiveRecord::Migration[7.0]
  def change
    create_table :service_types do |t|
      t.string :name
      t.integer :position
      t.string :message
      t.string :picto

      t.timestamps
    end
  end
end
