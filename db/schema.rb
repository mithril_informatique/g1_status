# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_08_26_002023) do
  create_table "histories", force: :cascade do |t|
    t.datetime "date"
    t.integer "service_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "duration"
    t.boolean "ping"
    t.integer "block"
    t.integer "error"
    t.string "version"
    t.integer "nb_documents"
    t.index ["service_id"], name: "index_histories_on_service_id"
  end

  create_table "service_types", force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.string "message"
    t.string "picto"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "services", force: :cascade do |t|
    t.string "url"
    t.boolean "status"
    t.integer "uptime"
    t.string "ipv4"
    t.string "ipv6"
    t.string "admin"
    t.string "server_funding"
    t.integer "service_type_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "port", default: 443
    t.string "version"
    t.boolean "ssl", default: true
    t.index ["service_type_id"], name: "index_services_on_service_type_id"
  end

  add_foreign_key "histories", "services"
  add_foreign_key "services", "service_types"
end
