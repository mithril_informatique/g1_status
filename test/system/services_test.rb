require "application_system_test_case"

class ServicesTest < ApplicationSystemTestCase
  setup do
    @service = services(:one)
  end

  test "visiting the index" do
    visit services_url
    assert_selector "h1", text: "Services"
  end

  test "should create service" do
    visit services_url
    click_on "New service"

    fill_in "Admin", with: @service.admin
    fill_in "Ipv4", with: @service.ipv4
    fill_in "Ipv6", with: @service.ipv6
    fill_in "Server funding", with: @service.server_funding
    fill_in "Service type", with: @service.service_type_id
    check "Status" if @service.status
    fill_in "Uptime", with: @service.uptime
    fill_in "Url", with: @service.url
    click_on "Create Service"

    assert_text "Service was successfully created"
    click_on "Back"
  end

  test "should update Service" do
    visit service_url(@service)
    click_on "Edit this service", match: :first

    fill_in "Admin", with: @service.admin
    fill_in "Ipv4", with: @service.ipv4
    fill_in "Ipv6", with: @service.ipv6
    fill_in "Server funding", with: @service.server_funding
    fill_in "Service type", with: @service.service_type_id
    check "Status" if @service.status
    fill_in "Uptime", with: @service.uptime
    fill_in "Url", with: @service.url
    click_on "Update Service"

    assert_text "Service was successfully updated"
    click_on "Back"
  end

  test "should destroy Service" do
    visit service_url(@service)
    click_on "Destroy this service", match: :first

    assert_text "Service was successfully destroyed"
  end
end
