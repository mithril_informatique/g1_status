Rails.application.routes.draw do
  resources :services
  resources :service_types

  root to: "welcome#index"
  get '/about' => "welcome#about"
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

end
