# README

Cette application permet de vérifier le bon fonctionnement des outils qui font tourner la June. Vous pouvez l'utiliser pour voir l'état des services comme Duniter, Cesium+, etc... 

## TODO

- [x] panneaux pour les différents logiciels
- [x] statut au moment de la visite
- [x] historique des statuts
- [x] Cesium+ pod
- [ ] lien vers le tutoriel d'installation
- [ ] statut de connexion en fonction de l'API
  - [x] duniter : version du noeud sur BMA `/`
  - [x] duniter : numéro du bloc courant sur BMA `/blockchain/current`
  - wotwizard
    - [x] version du noeud sur l'API GraphQL `{version}`
    - [ ] version de l'UI webapp wotwizard-ui
    - [ ] version de l'UI html wotwizard client
  - [x] cesium+ : nombre de documents `document/stats` 
  - [x] cesium+ : version sur `node/summary` 
  - [x] stocker la latence des requêtes sur les API